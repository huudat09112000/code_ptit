#include<stdio.h>
#include<string.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while(t--)
	{
		scanf ("\n");
		char s[200];
		gets(s);
		int t=0;
		for (int i=0;i<strlen(s);i++) t+=s[i]-'0';
		if (s[0]!='8'||s[strlen(s)-1]!='8') printf ("NO");
		else if (t%10!=0)
		{
			printf ("NO");
		}
		else 
		{
			int dem=0;
			for (int i=0;i<strlen(s);i++)
			if (s[i]!=s[strlen(s)-i-1])
			{
				printf ("NO");
				dem=1;
				break;
			}
			if (dem==0) printf ("YES");
		}
		printf ("\n");
	}
}