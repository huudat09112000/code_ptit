#include<stdio.h>
#include<math.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		long long n;
		scanf ("%lld",&n);
		int a[100],i=0,dem=0;
		while (n>0)
		{
			a[i]=n%10;
			dem++;
			n/=10;
			i++;
		}
		int lan=0;
		for (int i=0;i<dem;i++)
		{
			for (int j=i+1;j<dem;j++)
			{
				if (a[i]<a[j]) 
				{
					lan=1;
					break;
				}
			}
		}
		if (lan==0) printf ("YES\n");
		else printf ("NO\n");
	}
}