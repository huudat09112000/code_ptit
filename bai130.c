#include<stdio.h>
int nt(int n)
{
	for (int i=2;i*i<=n;i++) if (n%i==0) return 0;
	return 1;
}
int main ()
{
	int t;
	scanf ("%d",&t);
	while(t--)
	{
		int n;
		scanf ("%d",&n);
		int k=n/2;
		for (int i=2;i<=k;i++)
		{
			if (nt(i)&&nt(n-i)) printf ("%d %d ",i,n-i);
		}
		printf ("\n");
	}
}