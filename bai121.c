#include<stdio.h>
int tong(int n)
{
	int sum=0;
	while(n%2==0)
	{
		sum+=2;
		n/=2;
	}
	for (int i=3;i*i<=n;i++)
	{
		while (n%i==0)
		{
			if (i<10) sum+=i;
			else 
			{
				int t=i;
				while(t>0)
				{
					sum+=t%10;
					t/=10;
				}
			}
			n/=i;
		}
	}
	if (n>1)
	{
		while(n>0)
		{
			sum+=n%10;
			n/=10;
		}
	}
	return sum;
}
int main ()
{
	int n;
	scanf("%d",&n);
	int sum=0,m=n;
	while(m>0)
	{
		sum+=m%10;
		m/=10;
	}
	if (sum==tong(n)) printf ("YES");
	else printf ("NO");
}