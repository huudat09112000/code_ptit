#include<stdio.h>
int ucln(int a,int b)
{
	if(b==0) return a;
	return ucln(b,a%b);
}
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		int a,b;
		scanf ("%d%d",&a,&b);
		if (b>a)
		{
			int c=a;
			a=b;
			b=c;
		}
		printf ("%d\n",ucln(a,b));
	}
}