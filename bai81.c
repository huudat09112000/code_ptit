#include <stdio.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		long long a;
		scanf ("%lld",&a);
		int S=0;
		while (a>0)
		{
			int k=a%10;
			S+=k;
			a/=10;
		}
		if (S%10==0) printf ("YES\n");
		else printf ("NO\n");
	}
}