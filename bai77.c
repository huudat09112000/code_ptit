#include<stdio.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		long long n,S=0;
		scanf ("%lld",&n);
		while (n>0)
		{
			int k=n%10;
			S+=k;
			n/=10;
		}
		printf ("%lld\n",S);
	}
}