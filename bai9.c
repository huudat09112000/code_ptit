#include<stdio.h>
#include<math.h>
int main ()
{
	int a,b,c;
	scanf ("%d %d %d",&a,&b,&c);
	if (a==0)
	{
		if (b==0) printf ("NO");
		else printf ("%.2f ",-(float)c/b);
	}
	else 
	{
		float det;
		det=b*b-4*a*c;
		if (det<0) printf ("NO");
		if (det==0) printf ("%.2f ",-(float)b/2*a);
		if (det>0)
		{
			det=(float)sqrt(det);
			float x1,x2;
			x1=(-b+det)/(2*a);
			x2=(-b-det)/(2*a);
			printf ("%.2f %.2f",x1,x2);
		}
	}
}