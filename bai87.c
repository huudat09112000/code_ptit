#include<stdio.h>

int main ()
{
	int t;
	scanf ("%d",&t);
	for(int k=0;k<t;k++)
	{
		long long n;
		scanf ("%lld",&n);
		long long t=n;
		printf ("Test %d: ",k+1);
		for (int i=2;10*i<=n;i++)
		{
			int lan=0;
			if (t%i==0)
			{
				while (t%i==0&&t>0)
				{
					lan++;
					t/=i;
				}
				if (lan>0) printf ("%d(%d) ",i,lan);
			}
			else continue;
			if (t==0) break;
		}	
		printf ("\n");
	}
}