#include<stdio.h>
int uoc (int a,int b)
{
	if (a==0||b==0) return a+b;
	return uoc(b%a,a);
}
int main ()
{
	int t;
	scanf("%d",&t);
	while (t--)
	{
		int a,b,c,d;
		scanf("%d%d%d%d",&a,&b,&c,&d);
		if (uoc(a,b)==uoc(c,d)) printf ("YES\n");
		else printf ("NO\n");
	}
}