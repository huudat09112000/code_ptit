#include<stdio.h>
#include<math.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		long long n;
		scanf ("%lld",&n);
		if (n<2) printf ("NO");
		else if (n<4) printf ("YES");
		else 
		{
			int lan=0;
			for (int i=2;i<=sqrt(n);i++)
			{
				if (n%i==0)
				{
					lan=1;
					break;
				}
			}
			if (lan==0) printf ("YES\n");
			else printf ("NO\n");
		}
	}
}