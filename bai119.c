#include<stdio.h>
int tn(long long n)
{
	long long m=0,t=n;
	while(t>0)
	{
		m=m*10+t%10;
		t/=10;
	}
	if (m==n) return 1;
	return 0;
}
int ktr(long long n)
{
	int u;
	int sum=0;
	while(n>0)
	{
		u=n%10;
		if (u%2==0) return 0;
		sum+=u;
		n/=10;
	}
	if (sum%2!=0) return 1;
	return 0;
}
int main ()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		long long n;
		scanf("%lld",&n);
		if (ktr(n)&&tn(n)) printf ("YES\n");
		else printf ("NO\n");
	}
}