#include<stdio.h>
int ktr(int n)
{
	while (n>0)
	{
		int a=n%10;
		if (a!=2&&a!=3&&a!=5&&a!=7) return 0;
		n/=10;
	}
	return 1;
}
int ngto(int n)
{
	for (int i=2;i*i<=n;i++) if (n%i==0) return 0;
	return 1;
}
int main ()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		int dem=0;
		for (int i=a;i<=b;i++)
		{
			if (ktr(i)&&ngto(i)) dem++;
		}
		printf ("%d\n",dem);
	}
}