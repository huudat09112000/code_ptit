#include<stdio.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		long long n;
		scanf ("%lld",&n);
		int lan=0,dem=0;
		while (n>0)
		{
			int k=n%10;
			dem++;
			if (k==0||k==6||k==8) lan++;
			n/=10;
		}
		if (lan==dem) printf ("YES\n");
		else printf ("NO\n");
	}
}