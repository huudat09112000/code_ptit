#include<stdio.h>
int ktr(long long n)
{
	int chan=0,le=0;
	while(n>0)
	{
		int u=n%10;
		if (u%2==0) chan++;
		else le++;
		n/=10;
	}
	if (chan<=le) return 0;
	return 1;
}
int main ()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		long long n;
		scanf("%lld",&n);
		if (n%2==0)
		{
			if (ktr(n)) printf ("YES");
			else printf ("NO");
		}
		else printf ("NO");
		printf ("\n");
	}
}