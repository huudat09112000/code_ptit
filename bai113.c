#include<stdio.h>
int ktr(long long n)
{
	while (n>0)
	{
		int a=n%10;
		if (a%2!=0) return 0;
		n/=10;
	}
	return 1;
}
int main ()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		long long n;
		scanf("%lld",&n);
		if (n%2==0)
		{
			if (ktr(n)) printf ("YES");
			else printf ("NO");
		}
		else printf ("NO");
		printf ("\n");
	}
}