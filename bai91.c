#include<stdio.h>
#include<math.h>
int main ()
{
	int t;
	scanf ("%d",&t);
	while (t--)
	{
		long long a;
		scanf ("%lld",&a);
		int c=0,l=0;
		while (a>0)
		{
			int k=a%10;
			if (k%2==0) c++;
			else l++;
			a/=10;
		}
		printf ("%d %d\n",l,c);
	}
}