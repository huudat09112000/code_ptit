#include<stdio.h>
#include<math.h>
int main ()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n;
		scanf("%d",&n);
		if (n%2!=0) printf ("0\n");
		else 
		{
			int dem=1;
			for (int i=2;i*i<=n;i++)
			{
				if (n%i==0)
				{
					if (i%2==0) dem++;
					if ((n/i)%2==0) dem++;
				}
			}
			int a=sqrt(n);
			if (a*a==n) dem--;
			printf ("%d\n",dem);
		}
	}
}